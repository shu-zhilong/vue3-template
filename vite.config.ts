import {defineConfig, loadEnv} from 'vite'
import type {ConfigEnv, UserConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import viteCompression from 'vite-plugin-compression'
import {createSvgIconsPlugin} from 'vite-plugin-svg-icons' // vite-plugin-svg-icons 用于自动导入 svg 图标
import {visualizer} from 'rollup-plugin-visualizer'

// 路径查找
import {resolve} from 'path' //必须要引入resolve

export default defineConfig(({command, mode}: ConfigEnv): UserConfig => {
    // 根据当前工作目录中的 `mode` 加载 .env 文件
    // 设置第三个参数为 '' 来加载所有环境变量，而不管是否有 `VITE_` 前缀。
    const env = loadEnv(mode, process.cwd(), '')
    console.log(mode)
    return {
        base: env.VITE_BASE_PATH,
        // vite 配置
        plugins: [
            createSvgIconsPlugin({
                // 需要自动导入的 svg 文件目录（可自行修改）
                iconDirs: [resolve(process.cwd(), 'src/assets/icons/svg')],
                // 执行icon name的格式（可自行修改）
                symbolId: 'icon-[dir]-[name]'
            }),
            vue(),
            viteCompression({
                filter: /.(js|mjs|json|css|html|svg)$/i,
                verbose: true,
                disable: false,
                algorithm: 'gzip',
                ext: '.gz'
            }),
            visualizer({
                open: true
            })
        ],
        resolve: {
            alias: {
                '@': resolve(__dirname, './src')
            }
        },
        server: {
            open: true,
            port: 8888,
            host: '0.0.0.0',
            proxy: {
                '/api': {
                    target: 'http://192.168.110.171:8080',
                    changeOrigin: true,
                    rewrite: path => path.replace(/^\/api/, '')
                }
            }
        },
        css: {
            // 预处理器配置项
            preprocessorOptions: {
                less: {
                    math: 'always'
                }
            }
        },
        build: {
            sourcemap: false,
            terserOptions: {
                compress: {
                    drop_debugger: true,
                    drop_console: true
                }
            },
            cssCodeSplit: true,
            rollupOptions: {
                output: {
                    // 静态资源打包做处理
                    chunkFileNames: 'static/js/[name]-[hash].js',
                    entryFileNames: 'static/js/[name]-[hash].js',
                    assetFileNames: 'static/[ext]/[name]-[hash].[ext]',
                    manualChunks(id) {
                        if (id.includes('node_modules')) {
                            return id.toString().split('node_modules/')[1].split('/')[0].toString()
                        }
                    }
                }
            }
        }
    }
})
