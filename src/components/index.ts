// 全局注册组件，需要注册在这里面
import { type App } from 'vue'
import SvgIcon from './SvgIcon/index.vue'
import type { DefineComponent, Component } from 'vue'

//组件接口
interface Components {
  name: string,
  componentName: DefineComponent | Component
}

// 所有需要注册的组件
const components: Components [] = [
  { name: 'SvgIcon', componentName: SvgIcon }
]

export default {
  install(app: App): void {
    components.forEach((item: Components): void => {
      // 通过循环遍历数据批量注册组件
      app.component(item.name, item.componentName)
    })
  }
}
