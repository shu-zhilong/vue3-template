import { defineStore } from 'pinia'
import type { MenuList } from '@/layout/menu-item.vue'

interface State {
  menu: MenuList [],
  breadcrumb: string []
}

export const useMenuStore = defineStore('menuStore', {
  state: (): State => ({
    menu: [], // 菜单
    breadcrumb: [] // 面包屑
  })
})
