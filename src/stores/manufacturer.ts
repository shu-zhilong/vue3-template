import {defineStore} from 'pinia'
import {getAllvendor} from "@/api/manufacturer";

export const useManufacturerStore = defineStore('manufacturer', {
    state: () => ({
        manufacturer: [], // 厂商全部数据
    }),
    actions: {
        async getAllList() {
            const res = await getAllvendor()
            if (res.data.code === 200) {
                this.manufacturer = res.data.data
            } else {
                this.manufacturer = []
            }
        }
    }
})
