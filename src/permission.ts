import router from '@/router'
import {useNProgress} from '@/hooks/useNProgress'
import {getToken, removeToken} from "@/utils/auth";
import loading from "@/utils/loading"
import {ElMessage} from "element-plus";

const {start, done} = useNProgress()

router.beforeEach((to, from, next) => {
    loading.close()
    start()
    const title: any = to.meta.title
    if (title) {
        document.title = title
    }
    const token: string | null = getToken()
    const username: string | null = localStorage.getItem('username')
    if (token) {
        if (to.path === '/login') {
            next('/home')
        } else {
            if (username !=='admin') {
                removeToken()
                ElMessage.error('非admin用户无法使用')
                next('/login')
            } else {
                next()
            }
        }
    } else {
        if (to.path !== '/login') {
            next('/login')
            removeToken()
            ElMessage.error('用户未登录')
        } else {
            next()
        }
    }
})

router.afterEach(() => {
    done() // finish progress bar
})
