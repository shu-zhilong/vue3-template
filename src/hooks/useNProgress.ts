import { nextTick, type Ref, unref } from 'vue'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { useCssVar } from '@vueuse/core'

const primaryColor: Ref<string> = useCssVar('--el-color-primary', document.documentElement)

export const useNProgress = () => {
  NProgress.configure({ showSpinner: false })

  const initColor = async () => {
    await nextTick()
    setTimeout(() => {
      const bar: HTMLElement | null = document.querySelector('#nprogress .bar')
      if (bar) {
        bar.style.background = unref(primaryColor.value)
      }
    })
  }

  initColor()

  const start = (): void => {
    NProgress.start()
  }

  const done = (): void => {
    NProgress.done()
  }

  return {
    start,
    done
  }
}