import request from "@/utils/request"

// 查询软件信息
export const getSoftwareList = (params): Promise<any> => {
    return request.get('/rpa/manage/findList', {
        params: params
    })
}

// 新增或编辑软件的接口
export const saveOrUpdate = (data): Promise<any> => {
    return request.post('/rpa/manage/saveOrUpdate', data)
}

// 删除软件的接口
export const deleteSoftware = (id): Promise<any> => {
    return request.delete(`/rpa/manage/delete/${id}`)
}
