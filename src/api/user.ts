import request from "@/utils/request"
import qs from 'qs'

//登录接口参数
export interface loginParams {
    username: string,
    password: string,
    code: string
}
// 登录接口
export const loginApi = (data: loginParams):Promise<any> => {
    return request.post('/rpa/user/login', qs.stringify(data))
}

