import request from "@/utils/request"

// 查询厂商信息
export const getManufacturer = (params): Promise<any> => {
    return request.get('/rpa/vendor/findList', {
        params: params
    })
}

// 新增或编辑厂商的接口
export const saveOrUpdateVendor = (data): Promise<any> => {
    return request.post('/rpa/vendor/saveOrUpdate', data)
}

// 删除软件的接口
export const deleteVendor = (id): Promise<any> => {
    return request.delete(`/rpa/vendor/delete/${id}`)
}

// 全量查询
export const getAllvendor = (params): Promise<any> => {
    return request.get('/rpa/vendor/list', {
        params: params
    })
}
