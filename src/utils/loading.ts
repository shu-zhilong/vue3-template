import {ElLoading} from 'element-plus'
import {nextTick} from "vue";

interface Interface {
    loadingInstance: any

    show(content: string, target: string | HTMLElement): void

    close(): Promise<void>
}

class Loading implements Interface {
    loadingInstance: any

    /**
     * @description loading加载
     * @param {string} text 显示内容
     * @param {string | HTMLElement} target 插入目标选择器 | 元素
     * @return {void}
     */
    show(text: string = '加载中...', target: string | HTMLElement = '.main-router'): void {
        this.loadingInstance = ElLoading.service({
            lock: true,
            text: text,
            target: target,
            background: "rgba(0, 0, 0, 0.1)",
        })
    }

    // 关闭弹窗
    async close(): Promise<void> {
        if (this.loadingInstance) {
            await nextTick()
            this.loadingInstance.close()
        }
    }
}

export default new Loading()
