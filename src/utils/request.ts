import axios from 'axios'
import type {
    AxiosError,
    AxiosInstance,
    AxiosResponse,
    InternalAxiosRequestConfig
} from 'axios'
import {ElMessage} from 'element-plus'
import {getToken} from "@/utils/auth";

// 请求白名单，无须token的接口
const whiteList: string[] = ['/rpa/user/login']

// 创建axios实例
const service: AxiosInstance = axios.create({
    // axios中请求配置有baseURL选项，表示请求URL公共部分
    baseURL: import.meta.env.VITE_BASE_URL + import.meta.env.VITE_API_URL,
    // 超时
    timeout: 10000,
    withCredentials: true // Cookie
})

// 添加请求拦截器
service.interceptors.request.use((config: InternalAxiosRequestConfig) => {
    // 是否需要设置 token
    let isToken: boolean = whiteList.some((v: string): boolean => {
        if (!config.url) {
            return false
        }
        return config.url.indexOf(v) > -1
    })
    // 这里判断localStorage里面是否存在token，如果有则在请求头里面设置
    const token: string | null = getToken()
    if (token && !isToken) {
        config.headers.Authorization = `Bearer ${token}`
    }
    return config
}, (error: AxiosError) => {
    // 对请求错误做些什么
    return Promise.reject(error)
})

// 添加响应拦截器
service.interceptors.response.use((response: AxiosResponse<any>) => {
    const {code, message} = response.data
    // 返回code码不为200 就抛错
    if (code !== 200 && code !== 0) {
        ElMessage.error(message)
    }
    // 对响应数据做点什么
    return response
}, (error: AxiosError) => {
    // 对响应错误做点什么
    let {message} = error
    console.log(error)
    if (message === 'Network Error') {
        message = '后端接口连接异常'
    } else if (message.includes('timeout')) {
        message = '系统接口请求超时'
    } else if (message.includes('Request failed with status code')) {
        message = message = '系统接口' + message.substr(message.length - 3) + '异常'
    }
    ElMessage.error(message)
    return Promise.reject(error)
})

export default service
