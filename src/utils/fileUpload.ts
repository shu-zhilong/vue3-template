import COS from "cos-js-sdk-v5";
import type {UploadUserFile} from 'element-plus'

/*
* 上传文件到云存储
 */
export class UploadFile {
    fileList: UploadUserFile = [];
    file = null;
    cos = null;
    taskIdList: any[] = [];
    taskId: string = "";

    // file 或者 filelist
    constructor(file) {
        if (Array.isArray(file)) {
            this.fileList = file;
        } else {
            this.file = file;
        }
        this.cos = new COS({
            SecretId: 'AKIDoZCoQ1Y1PdgYMoAMRUTJ3d7ReicMOG0c',
            SecretKey: 'eDew3z6A7uDLH1aRGyWwJclrqmaFl9Pb'
            // // 获取临时秘钥
            // getAuthorization: async function (options, callback) {
            //     const res = await request.get('/cloud/file')
            //     const {
            //         credentials,
            //         TmpSecretId = credentials.tmpSecretId,
            //         TmpSecretKey = credentials.tmpSecretKey,
            //         SecurityToken = credentials.sessionToken,
            //         startTime,
            //         expiredTime
            //     } = res.data
            //     callback({
            //         TmpSecretId,
            //         TmpSecretKey,
            //         SecurityToken,
            //         // 建议返回服务器时间作为签名的开始时间，避免用户浏览器本地时间偏差过大导致签名错误
            //         StartTime: startTime, // 时间戳，单位秒，如：1580000000
            //         ExpiredTime: expiredTime, // 时间戳，单位秒，如：1580000000
            //     });
            // },
            // FileParallelLimit: 10
        })
    }

    /*
    * path 存储路径
    * uuid 存储文件名，防止覆盖
    * cb 参数为进度
    * */
    async uploadFiles(path: string, uuid: string = "", cb: Function) {
        let needUploadFile = this.fileList.map((item) => {
            return {
                Bucket: 'test-1313225720' /* 填入您自己的存储桶，必须字段 */,
                Region: "ap-nanjing" /* 存储桶所在地域，例如ap-beijing，必须字段 */,
                Key: `${path}${uuid + item.name}` /* 存储在桶里的对象键（例如1.jpg，a/b/test.txt），必须字段 */,
                Body: item.raw /* 必须，上传文件对象，可以是input[type="file"]标签选择本地文件后得到的file对象 */,
                onTaskReady: (taskId) => {
                    /* taskId可通过队列操作来取消上传cos.cancelTask(taskId)、停止上传cos.pauseTask(taskId)、重新开始上传cos.restartTask(taskId) */
                    this.taskIdList.push(taskId);
                }
            };
        });
        try {
            const data = await this.cos.uploadFiles({
                files: needUploadFile,
                SliceSize: 1024 * 1024 * 10 /* 设置大于10MB采用分块上传 */,
                onProgress: function (info) {
                    let percent = parseInt(info.percent * 10000) / 100;
                    let speed = parseInt((info.speed / 1024 / 1024) * 100) / 100;
                    cb(percent, speed);
                }
            });
            if (data.files.some(item => item.error)) {
                return {err: "上传云存储发生错误", data: null};
            } else {
                return {err: null, data: data};
            }
        } catch (err) {
            return {err: err, data: null};
        }
    }

    // 取消多文件上传
    cancelUploadFiles(): void {
        if (this.taskIdList.length !== 0) {
            this.taskIdList.forEach(item => {
                this.cos.cancelTask(item);
            });
        }
    }

    /*
     * path 存储路径
     * uuid 存储文件名，防止覆盖
     * cb 参数为进度
     * */
    async uploadFile(path: string, uuid: string = "", cb: Function) {
        try {
            const data = await this.cos.uploadFile({
                Bucket: 'test-1313225720' /* 填入您自己的存储桶，必须字段 */,
                Region: "ap-nanjing" /* 存储桶所在地域，例如ap-beijing，必须字段 */,
                Key: `${path}${uuid + this.file.raw.name}` /* 存储在桶里的对象键（例如1.jpg，a/b/test.txt），必须字段 */,
                Body: this.file.raw /* 必须，上传文件对象，可以是input[type="file"]标签选择本地文件后得到的file对象 */,
                SliceSize: 1024 * 1024 * 5,     /* 触发分块上传的阈值，超过5MB使用分块上传，非必须 */
                onTaskReady: (taskId) => {
                    this.taskId = taskId;
                },
                onProgress: function (info) {
                    let percent = parseInt(info.percent * 10000) / 100;
                    let speed = parseInt((info.speed / 1024 / 1024) * 100) / 100;
                    cb(percent, speed);
                }
            });
            return {err: null, data: {...data, Key: `${path}${uuid + this.file.raw.name}`}};
        } catch (err) {
            return {err: err, data: null};
        }
    }

    // 取消单文件上传
    cancelUploadFile(): void {
        if (this.taskId) {
            this.cos.cancelTask(this.taskId);
        }
    }

    // 删除文件
    deleteFiles(pathList): void {
        const Objects = pathList.map((item): { Key: string } => {
            return {
                Key: item
            }
        })
        this.cos.deleteMultipleObject({
            Bucket: 'test-1313225720', /* 填入您自己的存储桶，必须字段 */
            Region: 'ap-nanjing',  /* 存储桶所在地域，例如ap-beijing，必须字段 */
            Objects
        }, function (err, data): void {
            console.log(err, data)
        });
    }
}
