import * as CryptoJS from 'crypto-js'
import { secretKey } from '@/config/config'

export const encryptData = (data: string): string => {
  // 使用AES加密
  return CryptoJS.AES.encrypt(data, secretKey).toString()
}

export const decryptData = (ciphertext: string | null): string => {
  if (!ciphertext) return ''
  // 使用AES解密
  const bytes = CryptoJS.AES.decrypt(ciphertext, secretKey)
  return bytes.toString(CryptoJS.enc.Utf8)
}
