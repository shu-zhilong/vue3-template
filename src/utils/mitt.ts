import myMitt from 'mitt'
import type { Emitter } from 'mitt'
import { onBeforeUnmount } from 'vue'
import type { Fn } from '@vueuse/core'

export const mitt: Emitter<any> = myMitt()


export const useMitt = (name: string, callback: Fn): void => {
  if (name) {
    mitt.on(name, callback)

    onBeforeUnmount(() => {
      mitt.off(name)
    })
  }
}