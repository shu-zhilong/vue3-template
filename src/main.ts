import './assets/main.css'
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import localeZH from 'element-plus/es/locale/lang/zh-cn'
import 'element-plus/dist/index.css'
import './permission'
import "virtual:svg-icons-register"
import MyComponents from '@/components/index'
const app = createApp(App)

app.use(MyComponents)

app.use(ElementPlus, {
    locale: localeZH
})
app.use(createPinia())
app.use(router)

app.mount('#app')
