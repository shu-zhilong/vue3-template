import {createRouter, createWebHistory} from 'vue-router'
import type {RouteRecordRaw} from 'vue-router'
import Layout from '@/layout/index.vue'

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        redirect: () => '/login'
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('../views/login/login.vue'),
        meta: {
            title: '用户登录'
        }
    },
    {
        path: '/',
        component: Layout,
        children: [
            {
                path: 'home',
                name: 'home',
                component: () => import('../views/layout/home/home.vue'),
                meta: {
                    title: '首页'
                }
            },
            {
                path: 'user-manger',
                name: 'user-manger',
                component: () => import('../views/layout/user-manger/user-manger.vue'),
                meta: {
                    title: '用户管理'
                }
            },
            {
                path: 'software-info',
                name: 'software-info',
                component: () => import('../views/layout/software-info/software-info.vue'),
                meta: {
                    title: '软件信息'
                }
            },
            {
                path: 'add-software/:editInfo?/:isView?',
                name: 'add-software',
                component: () => import('../views/layout/add-software/add-software.vue'),
                meta: {
                    title: '查看(添加,编辑)软件',
                    activeMenu: '/software-info'
                }
            },
            {
                path: 'software-evaluation',
                name: 'software-evaluation',
                component: () => import('../views/layout/software-evaluation/software-evaluation.vue'),
                meta: {
                    title: '软件评分及评价'
                }
            },
            {
                path: 'manufacturer-manger',
                name: 'manufacturer-manger',
                component: () => import('../views/layout/manufacturer-manger/manufacturer-manger.vue'),
                meta: {
                    title: '厂商管理'
                }
            },
            {
                path: 'information-manger',
                name: 'information-manger',
                component: () => import('../views/layout/information-manger/information-manger.vue'),
                meta: {
                    title: '资讯管理'
                }
            }
        ]
    },
    {
        path: '/404',
        component: () => import('@/views/error/404.vue')
    },
    {
        path: '/:catchAll(.*)', // 匹配未定义的路由
        redirect: '/404' // 重定向
    }

]

const router = createRouter({
    history: createWebHistory(import.meta.env.VITE_BASE_PATH),
    routes,
    scrollBehavior: () => ({left: 0, top: 0})
})


// 定义一个resetRouter 方法，在退出登录后或token过期后 需要重新登录时，调用即可
export const resetRouter = (): void => {
    router.getRoutes().forEach((route) => {
        const {name} = route
        if (name) {
            router.hasRoute(name) && router.removeRoute(name)
        }
    })
}

export default router
