/// <reference types="vite/client" />
/// <reference types="vite-svg-loader" />
declare module "*.vue" {
    import { DefineComponent } from "vue"
    const component: DefineComponent<{}, {}, any>
    export default component
}

declare module 'crypto-js'
declare module 'lodash/throttle'
declare module 'nprogress'
